// import React from 'react'
import { ThemedSRC } from './App';
import { Flex, Dropdown, Modal, Divider } from 'antd';
import { useState } from 'react';
import { getThemedColors } from "./theme";


const SettingsDropdown = () => {
  const [modalOpen, setModalOpen] = useState(false);

  const [darkMode] = useState(false);
  const theme = getThemedColors(darkMode);

  const openManageAssetModal = () => {
    setModalOpen(true)
  };

  const createItem = (itemName, onClickHandler) => ({
    key: itemName,
    label: (
      <Flex align="center" justify="center">
        <button
          style={{
            padding: 0,
            color: theme.text,
            backgroundColor: 'transparent',
            border: 'none',
            textAlign: 'center',
            cursor: 'pointer'
          }}
          onClick={onClickHandler}
        >
          {itemName}
        </button>
      </Flex>
    )
  });

  const items = [createItem('Manage Asset', openManageAssetModal), createItem('Sensor Configuration')];

  return (
    <>
      <Dropdown menu={{ items, style: { backgroundColor: theme.background, color: theme.text  } }} placement="bottom">
        <button
          style={{ backgroundColor: 'transparent', border: 'none', margin: 0, padding: 0, cursor: 'pointer' }}
        >
          <img src={ThemedSRC('/icons/networkDevicesDetail/settings.svg', darkMode)} alt="report" width={24} height={24} />
        </button>
      </Dropdown>

      <Modal
        title={
          <Flex align="center" gap={8}>
            <h3 style={{ margin: 0, padding: 0, color: theme.text }}>Manage Asset</h3>
          </Flex>
        }
        centered
        className={darkMode ? 'delete-modal-dark' : 'delete-modal'}
        open={modalOpen}
        onCancel={() => setModalOpen(false)}
        footer={() => null}
      >
        <Flex vertical gap={4}>
            <label style={{ fontWeight: 700, fontSize: '14px', color: theme.text }} htmlFor="customId">
              Custom ID
            </label>
            <input
              style={{
                padding: '8px 12px',
                fontSize: '12px',
                backgroundColor: theme.background,
                border: '1px solid',
                borderColor: theme.hover,
                outline: 'none'
              }}
              id="customId"
              type="text"
              placeholder="Create a custom id"
            />
            <button
              style={{
                color: theme.modalAlert,
                fontWeight: 600,
                backgroundColor: 'transparent',
                border: 'none',
                textDecoration: 'underline',
                cursor: 'pointer',
                width: 'min-content',
                marginLeft: 'auto'
              }}
            >
              Save
            </button>
            <Divider style={{ margin: '4px 0px' }} />
            <Flex justify="space-between">
              <label htmlFor="addGroup" style={{ fontWeight: 700, fontSize: '14px', color: theme.text }}>
                Groups
              </label>
              <button
                id="addGroup"
                style={{
                  color: theme.modalAlert,
                  fontWeight: 600,
                  backgroundColor: 'transparent',
                  border: 'none',
                  textDecoration: 'underline',
                  cursor: 'pointer',
                  width: 'min-content'
                }}
              >
                Add
              </button>
            </Flex>
            <Divider style={{ margin: '4px 0px' }} />
            <Flex justify="space-between">
              <label htmlFor="addGroup" style={{ fontWeight: 700, fontSize: '14px', color: theme.text }}>
                Set Baseline
              </label>
              <button
                id="addGroup"
                style={{
                  color: theme.modalAlert,
                  fontWeight: 600,
                  backgroundColor: 'transparent',
                  border: 'none',
                  textDecoration: 'underline',
                  cursor: 'pointer',
                  width: 'min-content'
                }}
              >
                Enable
              </button>
            </Flex>
            <Divider style={{ margin: '4px 0px' }} />
            <Flex justify="space-between">
              <label htmlFor="addGroup" style={{ fontWeight: 700, fontSize: '14px', color: theme.text }}>
                Upload Firmware
              </label>
              <button
                id="addGroup"
                style={{
                  color: theme.modalAlert,
                  fontWeight: 600,
                  backgroundColor: 'transparent',
                  border: 'none',
                  textDecoration: 'underline',
                  cursor: 'pointer',
                  width: 'min-content'
                }}
              >
                Upload
              </button>
            </Flex>
            <Divider style={{ margin: '4px 0px' }} />
            <Flex justify="space-between">
              <label htmlFor="addGroup" style={{ fontWeight: 700, fontSize: '14px', color: theme.text }}>
                Store Original Data
              </label>
              <button
                id="addGroup"
                style={{
                  color: theme.modalAlert,
                  fontWeight: 600,
                  backgroundColor: 'transparent',
                  border: 'none',
                  textDecoration: 'underline',
                  cursor: 'pointer',
                  width: 'min-content'
                }}
              >
                Enable
              </button>
            </Flex>
          </Flex>
      </Modal>
    </>
  );
};

export default SettingsDropdown;

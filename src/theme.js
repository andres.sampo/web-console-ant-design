export const theme = {
    darkMode: {
        pass: "#49AA19",
        alert: "#D89614",
        fail: "#D32029",
        text: "#FFFFFFE0",
        background: "#2f2f37",
        breadItems: "#FFFFFFA6",
        breadLastItem: "#F8AA77",
        modalAlert: "#FF4E00",
        hover: "#50505D",
    },
    lightMode: {
        pass: "#135200",
        alert: "#FAAD14",
        fail: "#F5222D",
        text: "#000000E0",
        background: "#FFF",
        breadItems: "#000000A6",
        breadLastItem: "#8F2B00",
        modalAlert: "#FF4E00",
        hover: "#ececec",
    },
};

export const getThemedColors = (darkMode) => {
    if (darkMode) return theme.darkMode;
    else return theme.lightMode;
};

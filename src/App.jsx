/* eslint-disable react/prop-types */
import { Breadcrumb, Flex, Divider } from 'antd';
import { useState } from 'react';
import ManageAsset from './SettingsDropdown';
import { getThemedColors } from './theme';

export const ThemedSRC = (src, darkMode) => {
  if (darkMode) {
    const dotIndex = src.indexOf('.');

    if (dotIndex !== -1) {
      const partBefore = src.slice(0, dotIndex);
      const partAfter = src.slice(dotIndex);
      const result = partBefore + '-dm' + partAfter;
      return result;
    }
  }
  return src;
};

const CustomBreadcrumb = ({ items, darkMode }) => {
  const separatorStyle = { color: darkMode ? '#FFFFFFA6' : '#000000A6' };
  const lastItemStyle = { color: darkMode ? '#F8AA77' : '#8F2B00' };

  const formattedItems = items.map((item, index) => {
    const titleStyle = index === items.length - 1 ? lastItemStyle : separatorStyle;
    return {
      title: <span style={titleStyle}>{item}</span>
    };
  });

  return <Breadcrumb separator={<span style={separatorStyle}>/</span>} items={formattedItems} />;
};

const TitleSection = ({ darkMode }) => {
  return (
    <Flex gap="middle" align="center">
      <img src={ThemedSRC('/icons/networkDevicesDetail/arrow-left.svg', darkMode)} alt="arrow left" width={24} height={24} />
      <img src={ThemedSRC('/icons/networkDevicesDetail/wifi-strength-off.svg', darkMode)} alt="wifi icon" width={24} height={24} />
      <h1 style={{ fontSize: '30px', margin: '0', color: darkMode ? '#FFFFFFE0' : '#000000E0' }}>Cisco-3945-Portland-01</h1>
    </Flex>
  );
};

const ActionIcons = ({ darkMode, setDarkMode }) => {
  console.log('darkMode', darkMode);
  return (
    <Flex gap="middle" align="center">
      {/* TODO Remove this onclick */}
      <button style={{ backgroundColor: 'transparent', border: 'none', cursor: 'pointer', padding: 0 }} onClick={() => setDarkMode(!darkMode)}>
        <img src={ThemedSRC('/icons/networkDevicesDetail/file.svg', darkMode)} alt="settings" width={24} height={24} />
      </button>

      <ManageAsset />
      <img src={ThemedSRC('/icons/networkDevicesDetail/delete.svg', darkMode)} alt="delete" width={24} height={24} />
    </Flex>
  );
};

const BorderTitle = ({ darkMode, children, title }) => {
  return (
    <Flex
      gap={16}
      align="center"
      style={{ border: 0, borderBottom: 1, borderColor: '#cccccc', borderStyle: 'solid', padding: '4px', minHeight: '38px' }}
    >
      <h2 style={{ fontSize: '16px', margin: 0, padding: 0, color: darkMode ? '#FFFFFFE0' : '#000000E0' }}>{title}</h2>
      {children}
    </Flex>
  );
};

const StatusCard = ({ darkMode, title, value, icon }) => {
  return (
    <Flex
      align="center"
      justify="space-between"
      gap={8}
      vertical
      style={{ width: '100%', height: '85px', padding: '8px', backgroundColor: darkMode && '#26262C', borderRadius: '8px' }}
    >
      <h3 style={{ textAlign: 'center', color: darkMode ? '#FFFFFFE0' : '#000000E0', fontSize: '14px', margin: 0 }}>{title}</h3>
      <span
        style={{ textAlign: 'center', color: darkMode ? '#49AA19 ' : '#135200', display: 'flex', alignItems: 'center', fontSize: '12px', gap: '8px' }}
      >
        <img src={ThemedSRC(`/icons/networkDevicesDetail/${icon}.svg`, darkMode)} alt="shield-check" width={24} height={24} /> {value}
      </span>
    </Flex>
  );
};

function App() {
  const [darkMode, setDarkMode] = useState(false);
  const theme = getThemedColors(darkMode);

  return (
    <main style={{ width: '100%' }}>
      <Flex vertical gap={8} style={{ backgroundColor: darkMode ? '#2f2f37' : '#FFF', width: '100%', padding: '24px' }}>
        <CustomBreadcrumb items={['Asset List', 'Cisco-3945-Portland-01']} darkMode={darkMode} />
        <Flex justify="space-between">
          <TitleSection darkMode={darkMode} />
          <ActionIcons darkMode={darkMode} setDarkMode={setDarkMode} />
        </Flex>
      </Flex>
    </main>
  );
}

export default App;
